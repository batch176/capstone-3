import React from 'react';

//it creates a context object
//A context object, as the name state is a data type of an object that can be used to store information that can be shared to other components

const UserContext = React.createContext();

//Provider component -> allows the other component to consume/use the context object and supply necessary information needed to the context object

export const UserProvider = UserContext.Provider;

export default UserContext