import { useState, useContext, useEffect } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useParams, Link, useNavigate } from 'react-router-dom';


export default function SpecificProduct() {

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);


	useEffect(() => {

		console.log(productId)

		fetch(`https://capstone2-ecommerce-barcoma.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [])

	const { user } = useContext(UserContext);


	//Buy Function
	const order = (productId) => {

		fetch('https://capstone2-ecommerce-barcoma.herokuapp.com/orders/order', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: 'Successfully order!',
					icon: 'success',
					text: `You have successfully order for this ${ name } product`
				})

				navigate('/products')
			} else {
				Swal.fire({
					title: 'error!',
					icon: 'error',
					text: 'Something went wrong, please try again'
				})
			}
		})
	}

	return(
		<Container>
			<Card>
				<Card.Header>
					<h4>{ name }</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{ description }</Card.Text>
					<h6>Price: Php { price } </h6>
				</Card.Body>

				<Card.Footer>
				{ user.accessToken !== null ?
					<Button variant="primary" onClick={() => order(productId)}>Buy</Button>
					:
					<Button variant="warning" as={ Link } to="/login">Login to Enroll</Button>
				 }
					
					
				</Card.Footer>
			</Card>
		</Container>

		)
}
