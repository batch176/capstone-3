import React from "react";
import { Carousel } from "react-bootstrap";

const HeroSlider = () => {
    return (
        <Carousel fade className="carousel">
        <Carousel.Item>
          <img
            className="slide1 d-block"
            src="https://dlcdnwebimgs.asus.com/gain/1897F8AB-6AAF-44BA-AFB1-11A49A9F71D6/fwebp"
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="slide2 d-block"
            src="https://dlcdnwebimgs.asus.com/gain/CBED9885-4F99-4308-976B-9267F658C271/fwebp"
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="slide3 d-block"
            src="https://dlcdnwebimgs.asus.com/gain/C53DBABA-E249-45B9-86B6-969CB897EE86/fwebp"
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
    )
}

export default HeroSlider;