import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link } from 'react-router-dom';

const NavComponents = () => {
    return(
    <>
    <Navbar bg="dark" variant="dark">
        <Navbar.Brand><img className="Logo" src="https://static.tildacdn.com/tild3062-3139-4364-b731-343365643937/ROG_LOGO_GLOBAL-whit.png"/></Navbar.Brand>
        <Nav className="labels ms-auto">
        <Nav.Link as={Link} to='/'>Home</Nav.Link>
        <Nav.Link as={Link} to='/products'>Products</Nav.Link>
        <Nav.Link as={Link} to="/login">Login</Nav.Link>
        <Nav.Link as={Link} to="/register">Register</Nav.Link>
        </Nav>
    </Navbar>
    </>
    )
}

export default NavComponents;